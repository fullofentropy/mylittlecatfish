# mylittlecatfish

everyone loves the Catfish

- Create a new repository
- Clone the repository
- Create a branch
- Make 2 separate commits to that branch
- Push and merge your changes
- Delete the branch
- Create a 2nd new branch
- Make 2 separate commits to that branch
- Push and merge your changes
- Delete the branch
- Link the repository to this channel

<div>
<code>               _    __ _     _     
              | |  / _(_)   | |    
      ___ __ _| |_| |_ _ ___| |__  
     / __/ _` | __|  _| / __| '_ \ 
    | (_| (_| | |_| | | \__ \ | | |
     \___\__,_|\__|_| |_|___/_| |_|
</code>
</div>